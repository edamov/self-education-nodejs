var http = require('http')
    , fs = require('fs')
    , url = require('url')
    , host = '127.0.0.1'
    , port = 3000;

http.createServer(function (request, response) {
    var pathname = url.parse(request.url).pathname
        , extension = pathname.split('.').pop()
        , contentType
        , binary = ''
        , encoding = 'utf8';

    switch (extension) {
        case 'html':
            contentType = 'text/html';
            break;
        case 'css':
            contentType = 'text/css';
            break;
        case 'jpg':
            contentType = 'image/jpg';
            binary = 'binary';
            encoding = '';
            break;
    }

    if (pathname != '/favicon.ico') {
        var file = fs.readFileSync(__dirname + pathname, encoding);
        response.writeHead(200, {"Content-Type": contentType});
        response.write(file, binary);
    }
    response.end();
}).listen(port, host);