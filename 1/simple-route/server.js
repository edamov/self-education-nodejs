var http = require('http')
    , url = require('url')
    , host = '127.0.0.1'
    , port = 3000;

http.createServer(function (request, response) {
    var pathname = url.parse(request.url).pathname;

    console.log(pathname);

    response.writeHead(200, {"Content-Type": "text/plain"});

    switch (pathname) {
        case '/hello':
            response.write('Hellooooooooo');
            break;
        case '/bye':
            response.write('byeeeeeeeeee');
            break;
        default:
            response.write('What do you want?');
            break;
    }


    response.end();
}).listen(port, host);

