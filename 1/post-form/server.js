var http = require('http')
    , fs = require('fs')
    , url = require('url')
    , qs = require('querystring')
    , host = '127.0.0.1'
    , port = 3000;

fs.readFile(__dirname + '/index.html', function (err, html) {
    http.createServer(function (request, response) {
        var pathname = url.parse(request.url).pathname;

        response.writeHead(200, {"Content-Type": "text/html"});

        switch (pathname) {
            case '/':
                response.end(html);
                break;
            case '/post':
                var data = '';
                request.addListener("data", function(chunk) {
                    data += chunk;
                });

                request.addListener("end", function() {
                    var name = qs.parse(data).name;
                    response.end('Hello ' + name);
                });

                break;
            default:
                response.end('What do you want?');
                break;
        }
    }).listen(port, host);
});