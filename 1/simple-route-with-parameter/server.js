var http = require('http')
    , url = require('url')
    , host = '127.0.0.1'
    , port = 3000;

http.createServer(function (request, response) {
    var params = url.parse(request.url, true).query;

    response.writeHead(200, {"Content-Type": "text/plain"});

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            response.write(key + ': ' + params[key] + '\n');
        }
    }

    response.end();
}).listen(port, host);